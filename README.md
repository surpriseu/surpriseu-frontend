# SurpriseU
 Frontend for the gift search application.
 The application is not ready yet, so some functionality is not implemented.

#### Tech
- React
- Redux
- AtomicDesign
- StyledComponents
- Webpack
- Eslint
- Firebase

> Other commits can be seen in the previous version (fullstack app).
> View the [repository](https://bitbucket.org/mouire/surpriseu-fullstack/src/master/).
