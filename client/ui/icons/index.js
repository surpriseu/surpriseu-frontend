export { default as FemaleAvatar } from './female-avatar.svg'
export { default as MaleAvatar } from './male-avatar.svg'
export { default as CheckIcon } from './check.svg'

